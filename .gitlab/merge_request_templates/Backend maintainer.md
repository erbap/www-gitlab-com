/label ~backend ~"trainee maintainer"

<!-- Congratulations! Fill out the following MR when you feel you are ready to become -->
<!-- a backend maintainer! This MR should contain updates to `data/team.yml` -->
<!-- declaring yourself as a maintainer of the relevant application -->

- [ ] Mention `@gitlab-org/maintainers/rails-backend`, if not done (this issue template should do this automatically)
- [ ] Assign this issue to your manager

Trainee maintainer issue: <!-- Link to the trainee issue -->

### Overview

<!-- Overall experience at GitLab, how many merge requests authored, -->
<!-- gitlab-org projects at which already a maintainer -->

### Examples of reviews

<!-- Examples of reviews that hold the codebase to a high standard of quality -->

### Things to improve

<!-- Things to improve based on the feedback received during trainee maintainership -->

@gitlab-org/maintainers/rails-backend please chime in below with your thoughts, and
approve this MR if you agree.

## Once This MR is Merged

1. [ ] Request a maintainer from the `#backend_maintainers` Slack channel to add you as an Owner to `gitlab-org/maintainers/rails-backend`
1. [ ] Consider adding 'backend maintainer' to your [Slack notification keywords](https://slack.com/intl/en-gb/help/articles/201398467-Set-up-keyword-notifications)
1. [ ] Manager: Announce the good news in the relevant channels listed in https://about.gitlab.com/handbook/engineering/#keeping-yourself-informed
