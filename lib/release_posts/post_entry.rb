# frozen_string_literal: true

module ReleasePosts
  class PostEntry
    include Helpers

    attr_reader :options

    def initialize(options)
      @options = options
      @issue = Issue.new(options.issue_url)
      @reporter = reporter
    end

    def execute
      assert_feature_branch!
      assert_issue_url!
      assert_new_file!

      # Read type from $stdin unless it is already set
      options.type ||= InputParser.read_type
      assert_valid_type!

      $stdout.puts "\n\e[32mcreate\e[0m #{file_path}"
      $stdout.puts contents

      $stdout.puts "\n\e[34mhint\e[0m add a screenshot, commit, and push your changes!"
      $stdout.puts push_hint

      return if options.dry_run

      write

      system("#{editor} '#{file_path}'") if editor
    end

    private

    def contents
      return deprecation_content if options.type == 'deprecation'

      feature_content
    end

    def feature_content
      yaml_content = YAML.dump(
        'features' => {
          options.type => [
            {
              'name' => @issue.title,
              'available_in' => @issue.available_in,
              'gitlab_com' => true,
              'documentation_link' => 'https://docs.gitlab.com/ee/#amazing',
              'image_url' => '/images/unreleased/feature-a.png',
              'reporter' => reporter,
              'stage' => @issue.stage,
              'categories' => @issue.categories,
              'issue_url' => @issue.url,
              'description' => "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.\nPerferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n"
            }
          ]
        }
      )
      remove_trailing_whitespace(yaml_content)
    end

    def deprecation_content
      yaml_content = YAML.dump(
        'deprecations' => {
          'feature_name' => @issue.title,
          'documentation_link' => 'https://docs.gitlab.com/ee/#amazing',
          'reporter' => reporter,
          'due' => 'April 22, 2019',
          'issue_url' => @issue.url,
          'description' => "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.\nPerferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n"
        }
      )
      remove_trailing_whitespace(yaml_content)
    end

    def push_hint
      <<-HINT
      git add #{file_path}
      git commit -m "#{@issue.title}
      git push -u origin
      HINT
    end

    def write
      File.write(file_path, contents)
    end

    def editor
      ENV['EDITOR']
    end

    def assert_feature_branch!
      return unless branch_name == 'master'

      fail_with "Create a branch first!"
    end

    def assert_new_file!
      return unless File.exist?(file_path)
      return if options.force

      fail_with "#{file_path} already exists! Use `--force` to overwrite."
    end

    def assert_issue_url!
      return unless options.issue_url.empty?

      fail_with "Provide an issue URL for the release post item"
    end

    def assert_valid_type!
      return unless options.type && options.type == InputParser::INVALID_TYPE

      fail_with 'Invalid category given!'
    end

    def file_path
      base_path = File.join(
        unreleased_path,
        branch_name.gsub(/[^\w-]/, '-'))

      # Add padding for .yml extension
      base_path[0..MAX_FILENAME_LENGTH - 5] + '.yml'
    end

    def unreleased_path
      path = File.join('data', 'release_posts', 'unreleased')

      path
    end

    def branch_name
      @branch_name ||= capture_stdout(%w[git symbolic-ref --short HEAD]).strip
    end

    def reporter
      username = ENV['GITLAB_USERNAME'] || capture_stdout(%w[git config gitlab.username]).strip

      if username == ''
        $stdout.puts "\n\e[34mhint\e[0m configure your GitLab username to save time"
        $stdout.puts "    git config --global gitlab.username YOUR_USERNAME"

        username = 'pm1'
      end

      username
    end

    def remove_trailing_whitespace(yaml_content)
      yaml_content.gsub(/ +$/, '')
    end
  end
end
