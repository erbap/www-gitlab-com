---
layout: markdown_page
title: "Category Direction - Advanced Deployments"
---
 
- TOC
{:toc}

## Advanced Deployments 



### Incremental Rollout
 
One of the key sub-areas within Continuous Delivery that we're tracking
is incremental rollout. This is important because it enables unprecedented
control over how you deliver your software in a safe way.  For these
items we're also tagging the label "incremental rollout", and you can
see an issue list of these items [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIncremental%20Rollout).
Incremental rollout also serves as a key pillar for our [Progressive Delivery](/direction/ops/#progressive-delivery)
strategy.

### Blue/green deploys

Create two (nearly) identical environments, arbitrarily called blue and green. One isn't better than the other, just separate. With all traffic going to green, load new code onto blue, get it ready, and then switch the router to suddenly, as quickly as possible, send all new traffic to blue.

### Recreate in place

This type is a very simple deployment, all of the old pods are killed and replaced all at once with the new ones.

### Canary

In a Canary Release, you start with all your machines/dockers/pods at 100% of the current deployment. When you are ready to try out your new deployment on a subset of your users, you send those user's traffic to the new deployment, while the others are still on the current one. 

![alt text](/images/direction/cicd/canary.jpg)

## What's Next & Why

TBD

## Maturity Plan

TBD


## Top Customer Success/Sales Issue(s)
 
TBD

## Top Customer Issue(s)
 
TBD

## Top Internal Customer Issue(s)
 
TBD

### Delivery Team

TBD

## Top Vision Item(s)

TBD
