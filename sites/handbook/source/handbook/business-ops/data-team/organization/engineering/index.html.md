---
layout: handbook-page-toc
title: "Data Engineering Handbook"
description: "GitLab Data Engineering Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----

# Data Engineering at GitLab

The mission of the Data Engineering team is to build a secure and trusted data platform that makes it possible for **everyone to be an analyst** so that our *only* limitations are the data or analysts themselves. We do this **by means of our** [**GitLab values**](/handbook/values/) and our [**Data Team Principles**](/handbook/business-ops/data-team/#data-team-principles)

## Data Engineering Responsibilities

Of the [Data Team's Responsibilities](/handbook/business-ops/data-team/#responsibilities) the Data Engineering team is directly responsible for:

- Building and maintaining the company's central Enterprise Data Warehouse to support Reporting, Analysis, Dimensional Modeling, and Data Development for all GitLab teams
- Integrating new data sources to enable analysis of subject areas, activities, and processes
- Building and maintaining an Enterprise Dimensional Model to enable Single Source of Truth results
- Developing Data Management features such as master data, reference data, data quality, data catalog, and data publishing
- Providing Self-Service Data capabilities to help everyone leverage data and analytics
- Help to define and champion Data Quality practices and programs for GitLab data systems
- Providing customizable Data Services, including Data Modeling, Data Quality, and Data Integration

## Handbook First on the Data Engineering Team

At GitLab we are [Handbook First](/handbook/handbook-usage/#why-handbook-first). Any changes to our codebase or process should have a handbook MR created before the work is started or concurrently with the MR to change the code.

For example: If we are adding a data source, we would first create the merge request to update our [the extract and load](/handbook/business-ops/data-team/platform/#extract-and-load) and [system diagram](/handbook/business-ops/data-team/platform/infrastructure/#system-diagram) sections of our handbook. This MR can be updated as the change is created and merged along with the change. 