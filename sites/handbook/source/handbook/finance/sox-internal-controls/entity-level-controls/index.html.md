---
layout: handbook-page-toc
title: "Entity Level Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Risk Control Matrix

|Sl.no #|Process|Control Mapping
|:---|:---|:---|
|1|Integrity and ethical values|[C.01](https://about.gitlab.com/handbook/people-operations/code-of-conduct/) and [C.02](https://about.gitlab.com/handbook/people-operations/code-of-conduct/)
|2|Organisational structure|[C.03](https://docs.google.com/document/d/166XksiBM28zzAtsEpkHsFNLMAmz_lcFTxNPfg6WM204/edit?usp=sharing) and [C.04](https://about.gitlab.com/handbook/finance/authorization-matrix/)
|3|Board of Directors & Audit Committee|[C.05](https://about.gitlab.com/handbook/board-meetings/), [C.06](https://about.gitlab.com/handbook/board-meetings/#audit-committee-charter) and [C.07](https://about.gitlab.com/handbook/board-meetings/)
|4|Entity-wide objectives|[C.08](https://docs.google.com/document/d/1HALK81OatVCO7tRIj9YzDIaqQqmpCiomT6sT1fHrIIY/edit?usp=sharing)
|5|Risk Assessment|[C.09](https://about.gitlab.com/handbook/board-meetings/), C.10, [C.11](https://about.gitlab.com/handbook/finance/accounting/), [C.12](https://about.gitlab.com/handbook/tax/) and [C.16](https://about.gitlab.com/handbook/people-operations/code-of-conduct/)
|6|Monitoring|[C.13](https://about.gitlab.com/handbook/board-meetings/), [C.14](https://about.gitlab.com/handbook/engineering/security/#sts=Information%20Security%20Policies ) and [C.15](https://about.gitlab.com/handbook/engineering/development/enablement/geo/)
